1.- Desde el punto de vista de JavaScript la definición de un objeto es:
  a) una entidad independiente con propiedades y tipos
  b) un tipo de función especial
  c) una abstración de una cosa como una taza o un auto

2.- Que son las propiedades de un objeto?
  a) es el mecanismo que presenta JavaScript para interactuar con los objetos
  b) son similares a variables que estan asociadas a dicho objeto
  c) son funciones que modifican el comportamiento de un objeto

3.- Cuales son formas validas para acceder a las propiedades de un objeto?
  a) miObjeto.unaPropiedad;
  b) miObjeto.unaPropiedad();
  c) miObjeto['unaPropiedad'];

4.- Como puedes obtener una matriz con todos los nombres de las propiedades enumerables de un objeto?
  a) Object.keys(miObjeto);
  b) miObjeto.forEach();
  c) Object.getOwnPropertyNames(miObjeto);

5.- Evalua el siguiente snippet y elige la expresión que lo completa
    var miObjeto = {a:1, b:2, c:3};

    for (*ALGO FALTA AQUI*) {
      console.log("miObjeto." + unaPropiedad + " = " + miObjeto[unaPropiedad]);
    }

  a) var i = 0; i < miObjeto.length; i++
  b) unaPropiedad of miObjeto
  c) Object.keys(miObjeto)
  d) var unaPropiedad in miObjeto

6.- En JavaScript se dice que todo es un objeto, pero existen dos excepciones muy especificas, cuales son?
  a) null
  b) NaN
  c) Infinity
  d) undefined
