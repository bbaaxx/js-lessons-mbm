// Ejercicios

// Objetos 1

(function () {

  var persona = {
    // < FALTA ALGO AQUI >
  };

  var  automobil = {
    // < FALTA ALGO AQUI >
  };

  // Regresa: "vrooom"
  automobil.encender();

  // Regresa: "soy rico!"
  if ( automobil.precio > 1000 ) {
    persona.comprar(automobil);
  }

}());

(function() {
  var dameUnObjetoQueTenga = function(valor, llave) {
    // <FALTA ALGO AQUI>
  };

  // regresa { muchas: "cosas" }
  dameUnObjetoQueTenga('muchas', 'cosas');

}());
