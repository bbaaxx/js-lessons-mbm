// Import React
import React from "react";

// Import Spectacle Core tags
import {
  Appear,
  BlockQuote,
  Cite,
  CodePane,
  Deck,
  Fill,
  Heading,
  Image,
  Layout,
  Link,
  ListItem,
  List,
  Quote,
  Slide,
  Spectacle,
  Text,
  Table,
  TableRow,
  TableHeaderItem,
  TableItem
} from "spectacle";

// Import image preloader util
import preloader from "spectacle/lib/utils/preloader";

// Import theme
import createTheme from "spectacle/lib/themes/default";

// Require CSS
require("normalize.css");
require("spectacle/lib/themes/default/index.css");

const images = {
  city: require("../assets/city.jpg"),
  kat: require("../assets/kat.png"),
  teslaCovered: require("../assets/teslaCovered.jpg"),
  teslaHero: require("../assets/teslaHero.jpg"),
  logo: require("../assets/formidable-logo.svg"),
  markdown: require("../assets/markdown.png")
};

preloader(images);
const initialThemeDefinition = {
  primary: "#f44336",
  secondary: "#607D8B"
};
const initialTheme = createTheme(initialThemeDefinition);

export default class Presentation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      themeDefinition: initialThemeDefinition,
      theme: initialTheme
    };
    this.handleThemeChange = this.handleThemeChange.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
  }

  handleColorChange(evt) {
    evt.preventDefault();
    const newThemeDef = Object.assign({}, this.state.themeDefinition);
    newThemeDef[evt.target.name] = evt.target.value;
    this.setState({
      themeDefinition: newThemeDef
    });
  }

  handleThemeChange(evt) {
    evt.preventDefault();
    this.setState({
      theme: createTheme(this.state.themeDefinition)
    });
  }

  render() {
    return (
      <Spectacle theme={this.state.theme}>
        <Deck transition={["zoom", "slide"]} transitionDuration={500}>
          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={1} fit caps lineHeight={1} textColor="black">
              Objetos
            </Heading>
            <Heading size={1} caps>
              en JavaScript
            </Heading>
            <Heading size={1} fit caps textColor="black">
              como estructuras de datos
            </Heading>
            <Heading size={1} caps fit>
              y como el sistema nervioso central de JS
            </Heading>
          </Slide>

          <Slide
            transition={["zoom", "fade"]}
            bgColor="primary"
            notes="<ul><li>talk about that</li><li>and that</li></ul>"
          >
            <Heading size={1} caps fit textColor="white">
              1.- Los objetos representan matrices/vectores bi-dimensionales
            </Heading>
            <Text textSize=".8em" margin="20px 0px 0px" bold>
              Los programadores les llamamos cariñosamente: "vectores
              asociativos" porque asocian una llave con un valor dentro de un
              contenedor.
            </Text>

            <Table
              textColor="white"
              margin="20px auto"
              bgImage={images.teslaCovered.replace("/", "")}
            >
              <TableRow>
                <TableHeaderItem>Keys</TableHeaderItem>
                <TableHeaderItem>Values</TableHeaderItem>
              </TableRow>
              <TableRow>
                <TableItem>ejes</TableItem>
                <TableItem>2</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>llantas</TableItem>
                <TableItem>4</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>pasajeros</TableItem>
                <TableItem>5</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>tipoDeMotor</TableItem>
                <TableItem>electrico</TableItem>
              </TableRow>
              <TableRow>
                <TableItem>sonidoMotor</TableItem>
                <TableItem>vroom</TableItem>
              </TableRow>
            </Table>
          </Slide>

          <Slide
            transition={["zoom", "fade"]}
            bgColor="primary"
            notes="<ul><li>talk about that</li><li>and that</li></ul>"
          >
            <Heading size={1} caps fit textColor="white">
              2.- Los objetos son contenedores de propiedades
            </Heading>
            <Text textSize=".8em" margin="20px 0px 0px" bold>
              Las cuales por asociación, tienden a describir la abstracción de
              un solo concepto (sin embargo, esto no es estrictamente necesario)
            </Text>

            <CodePane
              lang="json"
              source={require("raw!../assets/simpleObject.example")}
              margin="40px 0"
              textSize=".8em"
            />
          </Slide>

          <Slide
            transition={["slide"]}
            bgImage={images.teslaHero.replace("/", "")}
            bgDarken={0.75}
          >
            <Heading size={1} caps textColor="primary">
              Es asi de
            </Heading>
            <Appear fid="2">
              <Heading size={1} caps fit textColor="tertiary">
                simple
              </Heading>
            </Appear>
            <Appear fid="3">
              <Heading size={1} caps textColor="black" bgColor="secondary">
                y elegante
              </Heading>
            </Appear>
          </Slide>

          <Slide transition={["zoom", "fade"]} bgColor="primary">
            <Heading caps fit>
              Como definir un objeto en JS
            </Heading>
            <Layout>
              <Fill>
                <Heading
                  size={4}
                  caps
                  textColor="secondary"
                  bgColor="white"
                  margin={10}
                >
                  Inicializador
                </Heading>
                <CodePane
                  lang="javascript"
                  source={require("raw!../assets/objectInitializer.example")}
                  margin="40px 0"
                />
              </Fill>
              <Fill>
                <Heading
                  size={4}
                  caps
                  textColor="secondary"
                  bgColor="white"
                  margin={10}
                >
                  Constructor
                </Heading>
                <CodePane
                  lang="javascript"
                  source={require("raw!../assets/objectConstructor.example")}
                  margin="40px 0"
                />
              </Fill>
            </Layout>
          </Slide>

          <Slide transition={["slide"]} bgColor="primary">
            <Heading size={1} caps fit textColor="tertiary">
              El tema de esta presentación esta definido en un objeto
            </Heading>
            <CodePane
              lang="json"
              source={JSON.stringify(this.state.themeDefinition)}
              textSize="0.8em"
              margin="40px 0"
            />
            <Link href="https://www.materialui.co/colors" target="_blank">
              Material UI colors
            </Link>
            <Layout margin="20px 0">
              <Fill>
                <Text>
                  Primary color:
                  <input
                    type="text"
                    name="primary"
                    value={this.state.themeDefinition.primary}
                    onChange={this.handleColorChange}
                  />
                </Text>
              </Fill>
              <Fill>
                <Text>
                  Secondary color:
                  <input
                    type="text"
                    name="secondary"
                    value={this.state.themeDefinition.secondary}
                    onChange={this.handleColorChange}
                  />
                </Text>
              </Fill>
            </Layout>
            <button onClick={this.handleThemeChange}>Cambiar tema</button>
          </Slide>

          <Slide transition={["spin", "zoom"]} bgColor="tertiary">
            <Heading caps fit size={1} textColor="primary">
              tres operaciones simples
            </Heading>
            <List ordered start={1} type="1">
              <ListItem>Acceder a las propiedades de un objeto (get)</ListItem>
              <ListItem>Cambiar las propiedades de un objeto (set)</ListItem>
              <ListItem>Iterar sobre el objeto</ListItem>
            </List>
          </Slide>

          <Slide transition={["spin", "zoom"]} bgColor="tertiary">
            <Heading caps fit size={1} textColor="primary">
              Acceder a las propiedades de un objeto
            </Heading>
            <CodePane
              lang="javascript"
              source={require("raw!../assets/propertyGet.example")}
              margin="40px 0"
            />
          </Slide>

          <Slide transition={["spin", "zoom"]} bgColor="tertiary">
            <Heading caps fit size={1} textColor="primary">
              Iterar sobre un objeto
            </Heading>
            <CodePane
              lang="javascript"
              source={require("raw!../assets/objectIteration.example")}
              margin="40px 0"
            />
          </Slide>

          <Slide transition={["spin", "zoom"]} bgColor="tertiary">
            <Heading caps fit size={1} textColor="primary">
              Iterar sobre un objeto (cheap mode)
            </Heading>
            <CodePane
              lang="javascript"
              source={require("raw!../assets/objectIterationCm.example")}
              margin="40px 0"
            />
          </Slide>

          <Slide transition={["slide"]} bgColor="black">
            <BlockQuote>
              <Quote>Todo en JavaScript es un objeto</Quote>
              <Cite>Some dude</Cite>
            </BlockQuote>
          </Slide>

          <Slide transition={["slide", "spin"]} bgColor="primary">
            <Heading caps fit size={1} textColor="tertiary">
              las propiedades de un objeto
            </Heading>
            <Heading caps fit size={1} textColor="secondary">
              pueden ser otros objetos !!!
            </Heading>
            <CodePane
              lang="javascript"
              source={`
                var unObjeto = {
                  otroObjeto: {foo: 'bar'}
                }
              `}
              margin="40px 0"
              textSize="0.8em"
            />
          </Slide>

          <Slide
            transition={["slide"]}
            bgColor="black"
            notes="You can even put notes on your slide. How awesome is that?"
          >
            <Image
              src={images.kat.replace("/", "")}
              margin="0px auto 40px"
              height="293px"
            />
            <Heading size={2} caps fit textColor="primary" textFont="primary">
              Wait what?
            </Heading>
          </Slide>

          <Slide transition={["slide", "spin"]} bgColor="primary">
            <Heading caps fit size={1} textColor="tertiary">
              En realidad, en un objeto
            </Heading>
            <Heading caps fit size={1} textColor="secondary">
              puedes almacenar cualquier tipo valido para JavaScript
            </Heading>
            <Heading caps fit size={1} textColor="black">
              incluyendo funciones
            </Heading>
            <CodePane
              lang="javascript"
              source={`
                var unObjeto = {
                  otroObjeto: {foo: 'bar'},
                  unaFuncion: function() { return 'Hola mundo'; }
                }
              `}
              textSize="0.8em"
              margin="40px 0"
            />
            <Text textSize=".8em" margin="20px 0px 0px" bold>
              * Las funciones almacenadas en objetos se llaman metodos
            </Text>
          </Slide>

          <Slide transition={["fade"]} bgColor="secondary" textColor="primary">
            <Appear>
              <Text textSize=".8em" margin="20px 0px 0px" bold>
                Debido a esta forma de implementar arreglos asociativos
              </Text>
            </Appear>
            <Appear>
              <Text>Todas las implementaciones de JavaScript</Text>
            </Appear>
            <Appear>
              <Text>Estan conformadas por un objeto complejo</Text>
            </Appear>
            <Appear>
              <Text>Compuesto por propiedades y metodos</Text>
            </Appear>
          </Slide>

          <Slide transition={["zoom"]} bgColor="primary">
            <Heading size={1} fit caps lineHeight={1} textColor="black">
              Pero eso
            </Heading>
            <Heading size={1} caps>
              lo veremos en
            </Heading>
            <Heading size={1} fit caps textColor="black">
              una lección
            </Heading>
            <Heading size={1} caps fit>
              mas avanzada
            </Heading>
          </Slide>

          <Slide transition={["spin", "slide"]} bgColor="tertiary">
            <Heading size={1} caps fit lineHeight={1.5} textColor="primary">
              Hecho con ❤ por:
            </Heading>
            <Link href="https://github.com/bbaaxx">
              Eduardo Mosqueda (bbaaxx)
            </Link>
          </Slide>
        </Deck>
      </Spectacle>
    );
  }
}
